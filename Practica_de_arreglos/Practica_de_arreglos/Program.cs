﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_de_arreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            Console.WriteLine("Escriba Un Numero Entre 1 y 9\n");
            opcion = Int32.Parse(Console.ReadLine());
            switch (opcion)
            {
                case 1:
                    pestaña1.cuatro_a_catorce();
                    break;
                case 2:
                    pestaña1.pares();
                    break;
                case 3:
                    pestaña1.div3();
                    break;
                case 4:
                    pestaña1.dosarreglos();
                    break;
                case 5:
                    pestaña1.sumadepares();
                    break;
                case 6:
                    pestaña1.cinconumeros();
                    break;
                case 7:
                    pestaña1.sumadenumeros();
                    break;
                case 8:
                    pestaña1.vocales();
                    break;
                case 9:
                    pestaña1.identicos();
                    break;
            }
        }
    }
}
