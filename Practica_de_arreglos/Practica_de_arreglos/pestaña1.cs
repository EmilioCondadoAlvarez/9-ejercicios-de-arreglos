﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_de_arreglos
{
    class pestaña1
    {
        public static void cuatro_a_catorce()
        {
            Console.WriteLine("Usaremos Un Arreglo Para Imprimir Los Numeros Del 4 a 14 De Forma Ascendente\n");
            int[] array = new int[15];
            for (int i = 4; i < 15; i++)
            {
                array[i] = i;
                Console.WriteLine("" + i);
            }
            Console.ReadKey();

        }
        public static void pares()
        {
            int par;
            Console.WriteLine("Usaremos Un Arreglo Para Imprimir Los Numeros Pares Entre 0 a 100\n");
            int[] array = new int[101];
            for (int i = 1; i < 101; i++)
            {
                array[i] = i;
                if ((i % 2) == 0)
                {
                    Console.WriteLine("" + i);
                }
            }
            Console.ReadKey();
        }
        public static void div3()
        {
            int par;
            Console.WriteLine("Usaremos Un Arreglo Para Imprimir Los Numeros Pares Entre 0 a 100\n");
            int[] array = new int[101];
            for (int i = 1; i < 101; i++)
            {
                array[i] = i;
                if ((i % 3) == 0)
                {
                    Console.WriteLine("" + i);
                }
            }
            Console.ReadKey();
        }
        public static void dosarreglos()
        {
            int[] uno = { 50, 55, 60, 65, 70, 75, 80, 85, 90, 95 };
            int[] dos = uno;
            double numeros = 0, multiplicacion = 0;
            for (int i=0; i<uno.Length; i++)
            {
                numeros = dos[i];
                multiplicacion = dos[i] * 0.5;
                Console.WriteLine("Uno De Los 10 Numeros Que Usamos Es:"+ numeros + "\n");
                Console.WriteLine(numeros + "Por 0.5 Es:\t" + multiplicacion + "\n");
            }
            Console.ReadKey();
        }
        public static void sumadepares()
        {
            Console.WriteLine("Usaremos Un Arreglo Para Imprimir La Suma De Los 20 Primeros Numeros Pares\n");
            double[] numeros = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };
            double suma = 0;
            for (int t = 0; t < numeros.Length; t++)
            {
                suma += numeros[t];
            }
            Console.WriteLine("La Suma De 2 + 4 + 6 + 8 + 10 + 12 + 14 +16 +18 + 20 Es:\t" + suma + "\t\n");
            Console.ReadKey();
        }
        public static void cinconumeros()
        {
            double a, b, c, d, e;
            Console.WriteLine("Usaremos Un Arreglo Para Saber La Media De 5 Numeros Que Usted Ingrese\n");
            Console.WriteLine("Escriba 5 Numeros");
            a = double.Parse(Console.ReadLine());
            b = double.Parse(Console.ReadLine());
            c = double.Parse(Console.ReadLine());
            d = double.Parse(Console.ReadLine());
            e = double.Parse(Console.ReadLine());
            double[] numeros = { a, b, c, d, e };
            double suma = 0, promedio;
            for (int t = 0; t < numeros.Length; t++)
            {
                suma += numeros[t];
            }
            promedio = suma / 5;
            Console.WriteLine("La Media De Los Numeros Es:\t" + promedio);
            Console.ReadKey();
        }
        public static void sumadenumeros()
        {
            Console.WriteLine("Usaremos Un Arreglo Para Sumar -2, -9, -4 y 5, 8, 10, 15\n");
            double[] numerosneg = {-2,-9,-4};
            double[] numerospos = { 5, 8, 10, 15 };
            double sumaneg = 0, sumapos = 0;
            for (int t = 0; t < numerosneg.Length; t++)
            {
                sumaneg += numerosneg[t];
            }
            for (int y = 0; y < numerospos.Length; y++)
            {
                sumapos += numerospos[y];
            }
            Console.WriteLine("La Suma De -2, -9, -4 Es:\t" + sumaneg + "\t\n");
            Console.WriteLine("La Suma De 5, 8, 10, 15 Es:\t" + sumapos + "\t\n");
            Console.ReadKey();
        }
        public static void vocales()
        {
            int vocales = 0;
            string palabra;
            Console.WriteLine("Escriba Una Palabra\n");
            palabra = Console.ReadLine();
            for (int i = 0; i<palabra.Length; i++)
            {
                if ((Convert.ToChar(palabra[i]))== 'a')
                {
                    vocales = vocales + 1;
                }
                if ((Convert.ToChar(palabra[i])) == 'e')
                {
                    vocales = vocales + 1;
                }
                if ((Convert.ToChar(palabra[i])) == 'i')
                {
                    vocales = vocales + 1;
                }
                if ((Convert.ToChar(palabra[i])) == 'o')
                {
                    vocales = vocales + 1;
                }
                if ((Convert.ToChar(palabra[i])) == 'u')
                {
                    vocales = vocales + 1;
                }
            }
            Console.WriteLine("Hay\t" + vocales + "\tVocales En La Palabra Que Introdujo\n");
            Console.ReadKey();
        }
        public static void identicos()
        {
            string a, b;
            int longitud = 0, c = 0;
            Console.WriteLine("Escriba Una Palabra");
            a = Console.ReadLine();
            Console.WriteLine("Escriba Otra Palabra");
            b = Console.ReadLine();
            string[] palabra1 = { a };
            string[] palabra2 = { b };
            if (palabra1.Length > palabra2.Length)
            {
                longitud = palabra1.Length;
            }
            if (palabra1.Length < palabra2.Length)
            {
                longitud = palabra2.Length;
            }
            if (palabra1.Length == palabra2.Length)
            {
                longitud = palabra1.Length;
            }
            for (int i = 0; i < longitud; i++)
            {
                if (palabra1[i] == palabra2[i])
                {
                    c = c + 1;
                }
            }
            if (c > 0)
            {
                Console.WriteLine("Las Palabras Que Introdujo Son Igules");
            }
            else
            {
                Console.WriteLine("Las Palabras Que Introdujo Son Diferentes");
            }
            Console.ReadKey();
        }
    }
}
